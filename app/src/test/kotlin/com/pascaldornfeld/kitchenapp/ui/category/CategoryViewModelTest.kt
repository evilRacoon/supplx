package com.pascaldornfeld.kitchenapp.ui.category

import android.database.sqlite.SQLiteConstraintException
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jraska.livedata.test
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.model.Category
import com.pascaldornfeld.kitchenapp.ui.MainDispatcherTestRule
import com.pascaldornfeld.kitchenapp.ui.category.CategoryViewModel.ErrorMessage
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.mockk
import kotlinx.coroutines.flow.flowOf
import org.junit.Rule
import org.junit.Test

class CategoryViewModelTest {
    private val categoryRepository =
        mockk<CategoryRepository>(relaxUnitFun = true) {
            every { getAllAscAsync() } returns flowOf()
        }

    private val categoryViewModel = CategoryViewModel(categoryRepository)

    @get:Rule val mainDispatcherTestRule = MainDispatcherTestRule()
    @get:Rule val instantTaskExecutor = InstantTaskExecutorRule()

    @Test
    fun createIfNotExists_shouldCreateNewCategory() {
        val category = Category("Asdf")
        categoryViewModel.createIfNotExists(category)
        mainDispatcherTestRule.dispatcher.scheduler.advanceUntilIdle()
        coVerify { categoryRepository.createIfNotExists(category) }
    }

    @Test
    fun swapSort_shouldSwapCategories() {
        val category = Category("Asdf")
        val category2 = Category("Asdff")
        categoryViewModel.swapSort(category, category2)
        mainDispatcherTestRule.dispatcher.scheduler.advanceUntilIdle()
        coVerify { categoryRepository.swapSort(category, category2) }
    }

    @Test
    fun delete_shouldDeleteCategory() {
        val category = Category("Asdf")
        categoryViewModel.delete(category)
        mainDispatcherTestRule.dispatcher.scheduler.advanceUntilIdle()
        coVerify { categoryRepository.delete(category) }
    }

    @Test
    fun changeName_shouldChangeName() {
        categoryViewModel.changeName("old", "new")
        mainDispatcherTestRule.dispatcher.scheduler.advanceUntilIdle()
        coVerify { categoryRepository.changeName("old", "new") }
    }

    @Test
    fun changeName_nameExists_shouldPostErrorMessage() {
        val errorMessages = categoryViewModel.errorMessage.test()

        coEvery { categoryRepository.changeName("old", "new") } throws
            SQLiteConstraintException("Name exists")
        categoryViewModel.changeName("old", "new")
        mainDispatcherTestRule.dispatcher.scheduler.advanceUntilIdle()

        errorMessages.assertValue(ErrorMessage.NameTaken)
    }
}
