package com.pascaldornfeld.kitchenapp.model

import com.pascaldornfeld.kitchenapp.domain.model.Category

data class UiCategory(val category: Category, val selected: Boolean)
