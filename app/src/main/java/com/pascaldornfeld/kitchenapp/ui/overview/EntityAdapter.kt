package com.pascaldornfeld.kitchenapp.ui.overview

import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity

class EntityAdapter(
    private val cardCallback: CardCallback,
    private val entityViewHolderFactory: EntityViewHolder.Factory,
) :
    ListAdapter<ItemEntity, EntityViewHolder>(
        object : DiffUtil.ItemCallback<ItemEntity>() {
            override fun areItemsTheSame(oldItem: ItemEntity, newItem: ItemEntity) =
                oldItem == newItem

            override fun areContentsTheSame(oldItem: ItemEntity, newItem: ItemEntity) = true
        }
    ) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EntityViewHolder =
        entityViewHolderFactory.create(parent)

    override fun onBindViewHolder(holder: EntityViewHolder, position: Int) =
        holder.bind(getItem(position)!!, cardCallback)

    fun deleteCategory(position: Int) = cardCallback.deleteItemEntity(getItem(position))
}
