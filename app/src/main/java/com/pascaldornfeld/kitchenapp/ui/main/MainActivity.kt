package com.pascaldornfeld.kitchenapp.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.pascaldornfeld.kitchenapp.ui.overview.OverviewActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        startActivity(Intent(this@MainActivity, OverviewActivity::class.java))
        finish()
    }
}
