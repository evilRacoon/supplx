package com.pascaldornfeld.kitchenapp.ui

import android.app.AlertDialog
import android.content.DialogInterface
import android.graphics.Canvas
import android.view.View
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView

class TouchHelperCallback(
    private val adapter: RecyclerView.Adapter<*>,
    private val deleteItCallback: DeleteItCallback,
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
    interface TouchHelperable {
        val viewForeground: View?
    }

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder,
    ): Boolean {
        return true
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        AlertDialog.Builder(viewHolder.itemView.context)
            .setTitle(deleteItCallback.deleteDialogTitle)
            .setMessage(deleteItCallback.deleteDialogText)
            .setIcon(android.R.drawable.ic_dialog_alert)
            .setPositiveButton(android.R.string.ok) { _: DialogInterface?, _: Int ->
                deleteItCallback.deleteIt(viewHolder)
            }
            .setNegativeButton(android.R.string.cancel) { _: DialogInterface?, _: Int ->
                adapter.notifyItemChanged(viewHolder.bindingAdapterPosition)
            }
            .setOnCancelListener { adapter.notifyItemChanged(viewHolder.bindingAdapterPosition) }
            .show()
    }

    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        if (viewHolder != null) {
            val foregroundView = (viewHolder as TouchHelperable).viewForeground
            ItemTouchHelper.Callback.getDefaultUIUtil().onSelected(foregroundView)
        }
    }

    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        val foregroundView = (viewHolder as TouchHelperable).viewForeground
        ItemTouchHelper.Callback.getDefaultUIUtil().clearView(foregroundView)
    }

    override fun onChildDrawOver(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
    ) {
        val foregroundView = (viewHolder as TouchHelperable).viewForeground
        ItemTouchHelper.Callback.getDefaultUIUtil()
            .onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive)
    }

    override fun onChildDraw(
        c: Canvas,
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        dX: Float,
        dY: Float,
        actionState: Int,
        isCurrentlyActive: Boolean,
    ) {
        val foregroundView = (viewHolder as TouchHelperable).viewForeground
        ItemTouchHelper.Callback.getDefaultUIUtil()
            .onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive)
    }

    interface DeleteItCallback {
        fun deleteIt(viewHolder: RecyclerView.ViewHolder)

        val deleteDialogTitle: String
        val deleteDialogText: String
    }
}
