package com.pascaldornfeld.kitchenapp.ui.edit

import android.text.Editable
import android.text.TextWatcher

internal abstract class TextWatcherAdapter : TextWatcher {
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {
        // not needed
    }

    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {
        // not needed
    }

    abstract override fun afterTextChanged(s: Editable)
}
