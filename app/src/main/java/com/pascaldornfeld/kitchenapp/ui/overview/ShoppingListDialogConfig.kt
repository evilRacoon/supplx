package com.pascaldornfeld.kitchenapp.ui.overview

import androidx.annotation.StringRes
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity

data class ShoppingListDialogConfig(
    val entity: ItemEntity,
    @StringRes val title: Int,
    @StringRes val text: Int,
    @StringRes val buttonText: Int,
) {
    companion object {
        fun createForMoveToShoppingList(entity: ItemEntity) =
            ShoppingListDialogConfig(
                entity,
                R.string.shopping_list_question,
                R.string.shopping_list_or_delete,
                R.string.shopping_list,
            )

        fun createForDelete(entity: ItemEntity) =
            ShoppingListDialogConfig(
                entity,
                R.string.keep_or_delete_question,
                R.string.keep_or_delete,
                R.string.keep,
            )
    }
}
