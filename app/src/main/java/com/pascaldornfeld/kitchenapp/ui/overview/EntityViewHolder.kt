package com.pascaldornfeld.kitchenapp.ui.overview

import android.app.AlertDialog
import android.content.DialogInterface
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.databinding.OverviewCardBinding
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.ui.TouchHelperCallback
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.time.temporal.ChronoUnit.DAYS

class EntityViewHolder(itemView: View) :
    RecyclerView.ViewHolder(itemView), TouchHelperCallback.TouchHelperable {
    private val binding = OverviewCardBinding.bind(itemView)

    class Factory {
        fun create(parent: ViewGroup) =
            EntityViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.overview_card, parent, false)
            )
    }

    override val viewForeground: View = binding.viewForeground

    companion object {
        const val STEPS = 4f
        const val FIRST_INDEX = -1
    }

    fun bind(itemEntity: ItemEntity, cardCallback: CardCallback) {
        binding.tvName.text = itemEntity.name
        val mhdCount = itemEntity.getCount()
        if (mhdCount == 0) {
            binding.tvCnt.visibility = View.GONE
            binding.tvMhd.visibility = View.GONE
            binding.imWarning.visibility = View.GONE
            binding.btUse.visibility = View.GONE
        } else {
            binding.tvCnt.visibility = View.VISIBLE
            binding.tvMhd.visibility = View.VISIBLE
            binding.btUse.visibility = View.VISIBLE
            binding.tvCnt.text =
                itemView.context.resources.getQuantityString(R.plurals.x_pieces, mhdCount, mhdCount)

            val days = DAYS.between(LocalDate.now(), itemEntity.getFirstExpiringMhd()!!).toInt()

            binding.tvMhd.text =
                itemView.context.resources.getQuantityString(
                    R.plurals.next_piece_has_x_days_left,
                    days,
                    days,
                )
            var alpha = -(1f / STEPS) * (days - FIRST_INDEX) + 1f
            if (alpha < 0f) binding.imWarning.visibility = View.INVISIBLE
            else {
                if (alpha > 1f) alpha = 1f
                binding.imWarning.alpha = alpha
                binding.imWarning.visibility = View.VISIBLE
            }
        }
        binding.btUse.setOnClickListener {
            AlertDialog.Builder(binding.btUse.context)
                .setTitle(R.string.confirm)
                .setMessage(
                    binding.btUse.context.getString(
                        R.string.do_you_want_to_remove_one_piece,
                        DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM)
                            .format(itemEntity.getFirstExpiringMhd()!!),
                    )
                )
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(android.R.string.ok) { _: DialogInterface?, _: Int ->
                    cardCallback.useItemEntity(itemEntity)
                }
                .setNegativeButton(android.R.string.cancel, null)
                .show()
        }
        binding.cdWhole.setOnClickListener { cardCallback.editItemEntity(itemEntity) }
    }
}
