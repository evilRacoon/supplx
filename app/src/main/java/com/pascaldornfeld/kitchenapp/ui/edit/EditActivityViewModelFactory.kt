package com.pascaldornfeld.kitchenapp.ui.edit

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.ItemEntityRepository
import com.pascaldornfeld.kitchenapp.util.StringResourceResolver

class EditActivityViewModelFactory(
    private val stringResourceResolver: StringResourceResolver,
    private val itemEntityRepository: ItemEntityRepository,
    private val categoryRepository: CategoryRepository,
    private val entitySavedName: String?,
    private val categoryName: String,
) : ViewModelProvider.Factory {
    override fun <T : ViewModel> create(modelClass: Class<T>): T {
        return if (modelClass.isAssignableFrom(EditActivityViewModel::class.java)) {
            @Suppress("UNCHECKED_CAST")
            EditActivityViewModel(
                stringResourceResolver,
                itemEntityRepository,
                categoryRepository,
                entitySavedName,
                categoryName,
            )
                as T
        } else throw Exception("unknown view model")
    }
}
