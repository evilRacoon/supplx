package com.pascaldornfeld.kitchenapp.ui.edit

import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.DialogInterface
import android.widget.DatePicker
import java.time.LocalDate

class MhdDatePicker(context: Context, private val model: EditActivityViewModel) :
    OnDateSetListener {
    private val appContext = context.applicationContext

    private val datePickerDialog = run {
        val today = LocalDate.now()
        DatePickerDialog(context, null, today.year, today.monthValue - 1, today.dayOfMonth)
    }

    init {
        setupPositiveButton()
        setupNegativeButton()
        datePickerDialog.show()
        disablePositiveButtonWhenDateCannotBePicked()
    }

    private fun disablePositiveButtonWhenDateCannotBePicked() {
        val datePicker = datePickerDialog.datePicker

        datePicker.setOnDateChangedListener {
            _: DatePicker?,
            year: Int,
            monthOfYear: Int,
            dayOfMonth: Int ->
            datePickerDialog.getButton(DialogInterface.BUTTON_POSITIVE).isEnabled =
                canPickDate(year, monthOfYear, dayOfMonth)
        }

        datePickerDialog.getButton(DialogInterface.BUTTON_POSITIVE).isEnabled =
            canPickDate(datePicker.year, datePicker.month, datePicker.dayOfMonth)
    }

    private fun setupNegativeButton() {
        datePickerDialog.setButton(
            DialogInterface.BUTTON_NEGATIVE,
            appContext.getString(android.R.string.cancel),
        ) { _: DialogInterface?, _: Int ->
        }
    }

    private fun setupPositiveButton() {
        datePickerDialog.setButton(
            DialogInterface.BUTTON_POSITIVE,
            appContext.getString(android.R.string.ok),
        ) { _: DialogInterface?, _: Int ->
            val datePicker = datePickerDialog.datePicker
            onDateSet(datePicker, datePicker.year, datePicker.month, datePicker.dayOfMonth)
        }
    }

    override fun onDateSet(view: DatePicker, year: Int, month: Int, dayOfMonth: Int) {
        val oldItem = model.itemEntityToEdit.value!!
        model.itemEntityToEdit.value = oldItem.plusMhd(LocalDate.of(year, month + 1, dayOfMonth))
        model.onChangingMhd()
    }

    private fun canPickDate(year: Int, monthOfYear: Int, dayOfMonth: Int): Boolean {
        val chosenDate = LocalDate.of(year, monthOfYear + 1, dayOfMonth)
        val listOfMhds = model.itemEntityToEdit.value!!.listOfMhds
        return listOfMhds.none { it.date == chosenDate }
    }
}
