package com.pascaldornfeld.kitchenapp.ui

import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.material3.TextField
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Modifier
import androidx.compose.ui.focus.FocusRequester
import androidx.compose.ui.focus.focusRequester
import androidx.compose.ui.platform.LocalSoftwareKeyboardController
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.TextRange
import androidx.compose.ui.text.input.ImeAction
import androidx.compose.ui.text.input.TextFieldValue
import androidx.compose.ui.tooling.preview.PreviewLightDark
import com.pascaldornfeld.kitchenapp.ui.theme.AppTheme

@Composable
fun EditTextAlertDialog(
    title: String,
    close: () -> Unit,
    onConfirm: (String) -> Unit,
    modifier: Modifier = Modifier,
    initialText: String = "",
) {

    var text by remember {
        mutableStateOf(TextFieldValue(initialText, TextRange(initialText.length)))
    }
    val dialogFocusRequester = remember { FocusRequester() }

    AlertDialog(
        modifier = modifier,
        onDismissRequest = { close() },
        confirmButton = {
            TextButton(
                onClick = {
                    onConfirm(text.text)
                    close()
                }
            ) {
                Text(stringResource(android.R.string.ok))
            }
        },
        dismissButton = {
            TextButton(onClick = { close() }) { Text(stringResource(android.R.string.cancel)) }
        },
        title = { Text(title) },
        text = {
            val keyboardController = LocalSoftwareKeyboardController.current
            TextField(
                modifier = Modifier.focusRequester(dialogFocusRequester),
                value = text,
                onValueChange = { text = it },
                keyboardOptions = KeyboardOptions(imeAction = ImeAction.Done),
                keyboardActions =
                    KeyboardActions(
                        onDone = {
                            keyboardController?.hide()
                            onConfirm(text.text)
                        }
                    ),
            )
            LaunchedEffect(Unit) { dialogFocusRequester.requestFocus() }
        },
    )
}

@PreviewLightDark
@Composable
private fun EditTextAlertDialogPreview() {
    AppTheme {
        EditTextAlertDialog(
            title = "Insert Text",
            close = {},
            onConfirm = {},
            initialText = "This is the text of the dialog",
        )
    }
}

@PreviewLightDark
@Composable
private fun EditTextAlertDialogPreview2() {
    AppTheme { EditTextAlertDialog(title = "Insert Text", close = {}, onConfirm = {}) }
}
