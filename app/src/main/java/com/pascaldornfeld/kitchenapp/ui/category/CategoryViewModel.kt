package com.pascaldornfeld.kitchenapp.ui.category

import android.database.sqlite.SQLiteConstraintException
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.asLiveData
import androidx.lifecycle.viewModelScope
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.model.Category
import com.pascaldornfeld.kitchenapp.util.SingleLiveEvent
import kotlinx.coroutines.launch

class CategoryViewModel(private val categoryRepository: CategoryRepository) : ViewModel() {
    val categories = categoryRepository.getAllAscAsync().asLiveData()

    private val mutableErrorMessage = SingleLiveEvent<ErrorMessage>()
    val errorMessage: LiveData<ErrorMessage> = mutableErrorMessage

    fun delete(category: Category) {
        viewModelScope.launch { categoryRepository.delete(category) }
    }

    fun changeName(oldName: String, newName: String) {
        viewModelScope.launch {
            try {
                categoryRepository.changeName(oldName, newName)
            } catch (exc: SQLiteConstraintException) {
                mutableErrorMessage.postValue(ErrorMessage.NameTaken)
            }
        }
    }

    fun createIfNotExists(category: Category) {
        viewModelScope.launch { categoryRepository.createIfNotExists(category) }
    }

    fun swapSort(category: Category, otherCategory: Category) {
        viewModelScope.launch { categoryRepository.swapSort(category, otherCategory) }
    }

    sealed interface ErrorMessage {
        data object NameTaken : ErrorMessage
    }

    companion object {
        class Factory(private val categoryRepository: CategoryRepository) :
            ViewModelProvider.Factory {
            @Suppress("UNCHECKED_CAST")
            override fun <T : ViewModel> create(modelClass: Class<T>): T {
                return CategoryViewModel(categoryRepository) as T
            }
        }
    }
}
