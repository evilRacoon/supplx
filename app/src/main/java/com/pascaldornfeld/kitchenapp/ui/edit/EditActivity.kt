package com.pascaldornfeld.kitchenapp.ui.edit

import android.app.AlertDialog
import android.content.DialogInterface
import android.os.Bundle
import android.text.Editable
import android.util.Pair
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.AdapterView
import android.widget.AdapterView.OnItemSelectedListener
import android.widget.ArrayAdapter
import android.widget.ImageButton
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.RecyclerView
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.databinding.EditActivityBinding
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.ui.edit.EditActivityViewModel.FinishStatus
import com.pascaldornfeld.kitchenapp.ui.edit.EditActivityViewModel.NameStatus
import com.pascaldornfeld.kitchenapp.ui.edit.EditActivityViewModel.UserFinishResponse
import com.pascaldornfeld.kitchenapp.ui.overview.OverviewActivity

class EditActivity : AppCompatActivity() {
    private val stringResourceResolver =
        com.pascaldornfeld.kitchenapp.di.Injector.stringResourceResolver
    private val categoryRepository = com.pascaldornfeld.kitchenapp.di.Injector.categoryRepository
    private val itemEntityRepository =
        com.pascaldornfeld.kitchenapp.di.Injector.itemEntityRepository

    private lateinit var mhdListAdapter: MhdListAdapter
    private lateinit var model: EditActivityViewModel
    private lateinit var binding: EditActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = EditActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Find Views
        val imAdd = findViewById<ImageButton>(R.id.im_addDate)
        val recyclerView = findViewById<RecyclerView>(R.id.recyclerView)

        // Setup ViewModel
        val intentEntity = intent.getStringExtra(OverviewActivity.EXTRA_ENTITY)

        val intentCategory =
            intent.getStringExtra(OverviewActivity.EXTRA_CATEGORY)
                ?: throw RuntimeException("Category is null")
        model =
            ViewModelProvider(
                this,
                EditActivityViewModelFactory(
                    stringResourceResolver,
                    itemEntityRepository,
                    categoryRepository,
                    intentEntity,
                    intentCategory,
                ),
            )[EditActivityViewModel::class.java]

        if (intentEntity == null) {
            setTitle(R.string.title_activity_create)
            binding.etName.setText("")
        } else {
            setTitle(R.string.title_activity_edit)
            binding.etName.setText(intentEntity)
            window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
        }
        addObserverButtonEnable()
        addObserverFinish()
        addObserverNameError()
        addObserverCategoriesData()

        // Setup EditText
        binding.etName.addTextChangedListener(
            object : TextWatcherAdapter() {
                override fun afterTextChanged(s: Editable) {
                    model.onChangingName(binding.etName.text.toString())
                }
            }
        )

        // Setup RecyclerView
        mhdListAdapter = MhdListAdapter(model)
        recyclerView.adapter = mhdListAdapter
        model.itemEntityToEdit.observe(this) { entity: ItemEntity? ->
            if (entity != null) mhdListAdapter.submitList(entity)
        }
        imAdd.setOnClickListener { MhdDatePicker(this@EditActivity, model) }
        binding.btSave.setOnClickListener {
            model.onFinishResponse(UserFinishResponse.FINISH_WITH_SAVE)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun addObserverCategoriesData() {
        model.categoriesData.observe(this) { categoriesData: Pair<Array<String>, Int>? ->
            if (categoriesData != null) {
                val adapter =
                    ArrayAdapter(this, android.R.layout.simple_spinner_item, categoriesData.first)
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                binding.categorySpinner.adapter = adapter
                binding.categorySpinner.setSelection(categoriesData.second!!)
                binding.categorySpinner.onItemSelectedListener =
                    object : OnItemSelectedListener {
                        override fun onItemSelected(
                            parent: AdapterView<*>?,
                            view: View?,
                            position: Int,
                            id: Long,
                        ) {
                            model.onChangingCategory(position)
                        }

                        override fun onNothingSelected(parent: AdapterView<*>?) {
                            throw RuntimeException("No category was selected")
                        }
                    }
            }
        }
    }

    /** Called, after changes on the item. Will enable or disable the save-button */
    private fun addObserverButtonEnable() {
        model.saveButtonEnabled.observe(this) { enabled: Boolean? ->
            if (enabled != null) binding.btSave.isEnabled = enabled
        }
    }

    /**
     * Called, when the user edits the items name. Based on validation, an error should be shown at
     * the EditText
     */
    private fun addObserverNameError() {
        model.nameError.observe(this) { error: NameStatus? ->
            when (error) {
                NameStatus.NAME_TAKEN ->
                    binding.etName.error = getString(R.string.name_already_taken)
                NameStatus.NAME_TOO_SHORT ->
                    binding.etName.error = getString(R.string.name_too_short)
                null -> binding.etName.error = null
            }
        }
    }

    /**
     * Called, when user wants to cancel or finish editing, f.e. back-button or save-button. Based
     * on the state, a dialog should be called or the activty should be finished
     */
    private fun addObserverFinish() {
        model.finishAction.observe(this) { finishStatus: FinishStatus? ->
            if (finishStatus != null) {
                when (finishStatus) {
                    FinishStatus.CAN_BE_SAVED ->
                        AlertDialog.Builder(this)
                            .setTitle(getString(R.string.save_question))
                            .setMessage(getString(R.string.do_you_save_changes))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.save) { _: DialogInterface?, _: Int ->
                                model.onFinishResponse(UserFinishResponse.FINISH_WITH_SAVE)
                            }
                            .setNegativeButton(R.string.do_not_save) { _: DialogInterface?, _: Int
                                ->
                                model.onFinishResponse(UserFinishResponse.FINISH_WITHOUT_SAVE)
                            }
                            .show()
                    FinishStatus.CANNOT_BE_SAVED ->
                        AlertDialog.Builder(this)
                            .setTitle(getString(R.string.continue_question))
                            .setMessage(getString(R.string.will_not_be_saved))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(R.string.do_not_save) { _: DialogInterface?, _: Int
                                ->
                                model.onFinishResponse(UserFinishResponse.FINISH_WITHOUT_SAVE)
                            }
                            .setNegativeButton(R.string.back, null)
                            .show()
                    FinishStatus.CAN_BE_SHOPPING_LISTED ->
                        AlertDialog.Builder(this)
                            .setTitle(getString(R.string.shopping_list_question))
                            .setMessage(getString(R.string.entry_shopping_listed))
                            .setIcon(android.R.drawable.ic_dialog_alert)
                            .setPositiveButton(application.getString(R.string.yes)) {
                                _: DialogInterface?,
                                _: Int ->
                                model.onFinishResponse(UserFinishResponse.FINISH_WITH_SHOPPING_LIST)
                            }
                            .setNegativeButton(application.getString(R.string.no)) {
                                _: DialogInterface?,
                                _: Int ->
                                model.onFinishResponse(UserFinishResponse.FINISH_WITH_SAVE_FORCE)
                            }
                            .show()
                    FinishStatus.FINISH -> finish()
                    FinishStatus.ERROR_OCCURRED ->
                        AlertDialog.Builder(this)
                            .setTitle(getString(R.string.error))
                            .setMessage(getString(R.string.error_occurred))
                            .setIcon(android.R.drawable.ic_dialog_info)
                            .setPositiveButton(android.R.string.ok, null)
                            .show()
                }
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onBackPressed() {
        super.onBackPressed()
        model.onBackClick()
    }
}
