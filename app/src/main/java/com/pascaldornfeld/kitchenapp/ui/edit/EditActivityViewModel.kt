package com.pascaldornfeld.kitchenapp.ui.edit

import android.database.sqlite.SQLiteConstraintException
import android.util.Pair
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.ItemEntityRepository
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.util.StringResourceResolver
import kotlinx.coroutines.launch

class EditActivityViewModel(
    private val stringResourceResolver: StringResourceResolver,
    private val itemEntityRepository: ItemEntityRepository,
    private val categoryRepository: CategoryRepository,
    private var entitySavedName: String?,
    categoryName: String,
) : ViewModel() {
    val saveButtonEnabled: MutableLiveData<Boolean> = MutableLiveData()
    val finishAction: MutableLiveData<FinishStatus> = MutableLiveData()
    val nameError: MutableLiveData<NameStatus?> = MutableLiveData()

    // 1) array of all categories. 2) category, which should be selected initially.
    val categoriesData: MutableLiveData<Pair<Array<String>, Int>?> = MutableLiveData()
    val itemEntityToEdit: MutableLiveData<ItemEntity> = MutableLiveData()
    private var currentEditingStatus: EditingStatus? = null
    private var changedSth = false
    private var otherEntityNames: List<String>? = null

    init {
        viewModelScope.launch {
            val categoryList = categoryRepository.getAllAsc()
            val categoryNames = categoryList.map { it.name }.toTypedArray()
            val current = categoryNames.indexOf(categoryName)
            if (current == -1) throw RuntimeException("Category not found")
            categoriesData.value = Pair(categoryNames, current)

            if (entitySavedName == null) {
                otherEntityNames = itemEntityRepository.getAll().map { it.name }
                itemEntityToEdit.setValue(ItemEntity("", categoryName, ArrayList()))
            } else {
                val currentEntityAndOthers =
                    itemEntityRepository.getAll().groupBy { it.name == entitySavedName }
                otherEntityNames = currentEntityAndOthers[false]?.map { it.name } ?: emptyList()
                val saved = currentEntityAndOthers[true]?.single()
                @Suppress("UNNECESSARY_NOT_NULL_ASSERTION")
                if (saved != null) itemEntityToEdit.setValue(saved!!)
                else itemEntityToEdit.setValue(ItemEntity("", categoryName, ArrayList()))
            }
            updateUI(true)
        }
    }

    fun onChangingName(newName: String?) {
        changedSth = true
        val ie = itemEntityToEdit.value
        if (ie != null) {
            itemEntityToEdit.value = ie.copy(name = newName!!)
            updateUI(true)
        }
    }

    fun onChangingCategory(indexInArray: Int) {
        if (categoriesData.value == null) throw RuntimeException("CategoriesData value is null")
        if (categoriesData.value!!.second != indexInArray) changedSth = true
        val ie = itemEntityToEdit.value
        if (ie != null)
            itemEntityToEdit.value =
                ie.copy(categoryName = categoriesData.value!!.first[indexInArray])
    }

    fun onChangingMhd() {
        changedSth = true
        updateUI(false)
    }

    fun onBackClick() {
        if (changedSth)
            if (
                currentEditingStatus == EditingStatus.VALID ||
                    currentEditingStatus == EditingStatus.VALID_NO_MHDS
            )
                finishAction.setValue(FinishStatus.CAN_BE_SAVED)
            else finishAction.setValue(FinishStatus.CANNOT_BE_SAVED)
        else finishAction.setValue(FinishStatus.FINISH)
    }

    /** The user wants to finish the activity, so check which dialog should be shown if any */
    fun onFinishResponse(response: UserFinishResponse) {
        try {
            if (
                response == UserFinishResponse.FINISH_WITH_SAVE ||
                    response == UserFinishResponse.FINISH_WITH_SAVE_FORCE
            ) {
                if (saveItem(response)) return
            } else if (response == UserFinishResponse.FINISH_WITH_SHOPPING_LIST) {
                moveItemToShoppingList()
            }
        } catch (exce: SQLiteConstraintException) {
            finishAction.value = FinishStatus.ERROR_OCCURRED
            return
        }
        finishAction.value = FinishStatus.FINISH
    }

    private fun saveItem(response: UserFinishResponse): Boolean {
        val finalItem = itemEntityToEdit.value!!
        // just save it or show the dialog to change to shopping list
        if (currentEditingStatus == EditingStatus.VALID_NO_MHDS) {
            // if shopping list is set as cat: just save it. otherwise dialog
            if (
                response != UserFinishResponse.FINISH_WITH_SAVE_FORCE &&
                    finalItem.categoryName !=
                        stringResourceResolver.resolveString(R.string.shopping_category)
            ) {
                finishAction.value = FinishStatus.CAN_BE_SHOPPING_LISTED
                return true
            }
        } else if (currentEditingStatus != EditingStatus.VALID)
            throw RuntimeException(
                "EditingStatus is not valid as expected: " + currentEditingStatus!!.name
            )
        val nameOfOldItem = entitySavedName
        viewModelScope.launch {
            if (nameOfOldItem != null) itemEntityRepository.update(nameOfOldItem, finalItem)
            else itemEntityRepository.save(finalItem)
        }
        entitySavedName = finalItem.name
        return false
    }

    private fun moveItemToShoppingList() {
        // user wants to shopping list it. Change category and save
        val wrongCatItem = itemEntityToEdit.value!!
        val finalItem =
            wrongCatItem.copy(
                categoryName = stringResourceResolver.resolveString(R.string.shopping_category)
            )
        val nameOfOldItem = entitySavedName
        viewModelScope.launch {
            if (nameOfOldItem != null) itemEntityRepository.update(nameOfOldItem, finalItem)
            else itemEntityRepository.save(finalItem)
        }
    }

    private val isValid: EditingStatus
        get() {
            val (name, _, arrayListMhd) = itemEntityToEdit.value!!
            return when {
                name.length < 2 -> EditingStatus.NAME_TOO_SHORT
                otherEntityNames!!.contains(name) -> EditingStatus.NAME_TAKEN
                arrayListMhd.isEmpty() -> EditingStatus.VALID_NO_MHDS
                else -> EditingStatus.VALID
            }
        }

    private fun updateUI(textChanged: Boolean) {
        currentEditingStatus = isValid
        if (textChanged) {
            when (currentEditingStatus) {
                EditingStatus.NAME_TAKEN -> nameError.setValue(NameStatus.NAME_TAKEN)
                EditingStatus.NAME_TOO_SHORT -> nameError.setValue(NameStatus.NAME_TOO_SHORT)
                else -> nameError.setValue(null)
            }
        }
        if (
            currentEditingStatus == EditingStatus.VALID ||
                currentEditingStatus == EditingStatus.VALID_NO_MHDS
        ) {
            saveButtonEnabled.setValue(true)
        } else saveButtonEnabled.setValue(false)
    }

    private enum class EditingStatus {
        VALID,
        VALID_NO_MHDS,
        NAME_TAKEN,
        NAME_TOO_SHORT,
    }

    enum class NameStatus {
        NAME_TAKEN,
        NAME_TOO_SHORT,
    }

    enum class FinishStatus {
        FINISH,
        ERROR_OCCURRED,
        CAN_BE_SAVED,
        CANNOT_BE_SAVED,
        CAN_BE_SHOPPING_LISTED,
    }

    enum class UserFinishResponse {
        FINISH_WITH_SAVE,
        FINISH_WITH_SAVE_FORCE,
        FINISH_WITHOUT_SAVE,
        FINISH_WITH_SHOPPING_LIST,
    }
}
