package com.pascaldornfeld.kitchenapp.ui.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.asLiveData
import androidx.lifecycle.map
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.ItemEntityRepository
import com.pascaldornfeld.kitchenapp.domain.model.Category
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.model.UiCategory
import com.pascaldornfeld.kitchenapp.util.combineWith

class OverviewDataProvider(
    itemEntityRepository: ItemEntityRepository,
    categoryRepository: CategoryRepository,
    private val createDefaultCategory: () -> Unit,
) {
    private val rawItems = itemEntityRepository.getAllAsync().asLiveData()
    private val rawCategories =
        categoryRepository.getAllAscAsync().asLiveData().createDefaultCategoryIfEmpty()

    private fun LiveData<List<Category>>.createDefaultCategoryIfEmpty() = map {
        if (it.isEmpty()) createDefaultCategory()
        it
    }

    private val selectedCategoryNameInput: MutableLiveData<String?> = MutableLiveData(null)
    val selectedCategoryName = selectedCategoryNameInput.adjustToAvailableCategories()

    private fun LiveData<String?>.adjustToAvailableCategories() =
        combineWith(rawCategories) { selected, categories ->
            if (categories.isNullOrEmpty()) null
            else if (!categories.map { it.name }.contains(selected)) categories.first().name
            else selected
        }

    val categories = rawCategories.mapToUiModel()

    private fun LiveData<List<Category>>.mapToUiModel() =
        combineWith(selectedCategoryName) { categories, selectedName ->
            categories?.map { UiCategory(it, it.name == selectedName) } ?: emptyList()
        }

    val currentItemEntities = rawItems.filterBySelectedCategory().sort()

    private fun <T : Comparable<T>> LiveData<List<T>>.sort() = map { it.sorted() }

    private fun LiveData<List<ItemEntity>>.filterBySelectedCategory() =
        combineWith(selectedCategoryName) { items, selectedCategory ->
            if (selectedCategory == null || items == null) emptyList()
            else items.filter { it.categoryName == selectedCategory }
        }

    fun selectCategory(cat: UiCategory) {
        selectedCategoryNameInput.postValue(cat.category.name)
    }
}
