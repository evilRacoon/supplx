package com.pascaldornfeld.kitchenapp.ui.overview

import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.SparseArray
import android.view.Menu
import android.view.MenuItem
import android.view.SubMenu
import android.view.View
import androidx.activity.viewModels
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.navigation.NavigationView
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.databinding.OverviewActivityBinding
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.model.UiCategory
import com.pascaldornfeld.kitchenapp.ui.TouchHelperCallback
import com.pascaldornfeld.kitchenapp.ui.TouchHelperCallback.DeleteItCallback
import com.pascaldornfeld.kitchenapp.ui.category.CategoryActivity
import com.pascaldornfeld.kitchenapp.ui.edit.EditActivity
import timber.log.Timber

class OverviewActivity :
    AppCompatActivity(), NavigationView.OnNavigationItemSelectedListener, CardCallback {
    private val entityViewHolderFactory = EntityViewHolder.Factory()
    private val viewModel by viewModels<OverviewViewModel>()

    lateinit var entityAdapter: EntityAdapter

    private var categoryIdMap: SparseArray<UiCategory> = SparseArray()
    private lateinit var categoryMenu: SubMenu
    private lateinit var binding: OverviewActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = OverviewActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setSupportActionBar(binding.overviewContent.toolbar)
        val toggle =
            ActionBarDrawerToggle(
                this,
                binding.drawerLayout,
                binding.overviewContent.toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close,
            )
        binding.drawerLayout.addDrawerListener(toggle)
        toggle.syncState()

        // generate categories in navigation drawer
        categoryMenu = binding.navigationView.menu.findItem(R.id.item_category).subMenu!!
        binding.navigationView.setNavigationItemSelectedListener(this)

        // rest
        entityAdapter = EntityAdapter(this, entityViewHolderFactory)
        binding.overviewContent.recyclerView.adapter = entityAdapter

        // SwipeToDelete-TouchHelper
        val itemTouchHelper =
            ItemTouchHelper(
                TouchHelperCallback(
                    entityAdapter,
                    object : DeleteItCallback {
                        override fun deleteIt(viewHolder: RecyclerView.ViewHolder) =
                            entityAdapter.deleteCategory(viewHolder.bindingAdapterPosition)

                        override val deleteDialogTitle: String
                            get() = getString(R.string.delete_question)

                        override val deleteDialogText: String
                            get() = getString(R.string.entry_will_be_deleted)
                    },
                )
            )
        itemTouchHelper.attachToRecyclerView(binding.overviewContent.recyclerView)
        setupItemAndCheckedCatObserver()

        // floating button for add-new-item
        viewModel.selectedCategoryName.observe(this) { selectedCategoryName ->
            binding.overviewContent.floatingButtonAdd.setOnClickListener {
                val intent = Intent(this, EditActivity::class.java)
                intent.putExtra(EXTRA_CATEGORY, selectedCategoryName)
                startActivity(intent)
            }
        }
    }

    private fun setupItemAndCheckedCatObserver() {
        viewModel.currentItemEntities.observe(this) { entityAdapter.submitList(it) }
        viewModel.categories.observe(this) {
            it.find { it.selected }?.let { title = it.category.name }

            val localCategoryIdMap = SparseArray<UiCategory>()
            categoryIdMap = localCategoryIdMap
            categoryMenu.clear()
            for (cat in it) {
                val id: Int = View.generateViewId()
                localCategoryIdMap.put(id, cat)
                categoryMenu
                    .add(R.id.group_category, id, Menu.NONE, cat.category.name)
                    .setIcon(R.drawable.icon_bookmark_gray)
                    .apply { isChecked = cat.selected }
            }
            categoryMenu.setGroupCheckable(R.id.group_category, true, true)
        }
        viewModel.shoppingListDialogConfig.observe(this) {
            AlertDialog.Builder(this)
                .setTitle(it.title)
                .setMessage(it.text)
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setPositiveButton(it.buttonText) { _: DialogInterface?, _: Int ->
                    viewModel.moveToShoppingList(it.entity)
                }
                .setNegativeButton(R.string.delete) { _: DialogInterface?, _: Int ->
                    viewModel.deleteItem(it.entity)
                }
                .show()
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        if (item.itemId == R.id.edit_category)
            startActivity(Intent(this, CategoryActivity::class.java))
        else {
            val cat = categoryIdMap[item.itemId]
            if (cat != null) viewModel.setActiveCategory(cat) else Timber.e("unknown navitem")
        }
        binding.drawerLayout.closeDrawer(GravityCompat.START)
        return true
    }

    override fun useItemEntity(entity: ItemEntity) {
        viewModel.useItem(entity)
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START))
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        else super.onBackPressed()
    }

    override fun editItemEntity(entity: ItemEntity) {
        val intent = Intent(this, EditActivity::class.java)
        intent.putExtra(EXTRA_ENTITY, entity.name)
        intent.putExtra(EXTRA_CATEGORY, entity.categoryName)
        startActivity(intent)
    }

    override fun deleteItemEntity(entity: ItemEntity) {
        viewModel.deleteItem(entity)
    }

    companion object {
        const val EXTRA_ENTITY = "entity"
        const val EXTRA_CATEGORY = "category"
    }
}
