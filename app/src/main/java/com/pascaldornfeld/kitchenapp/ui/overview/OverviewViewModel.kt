package com.pascaldornfeld.kitchenapp.ui.overview

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.di.Injector
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.ItemEntityRepository
import com.pascaldornfeld.kitchenapp.domain.model.Category
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.model.UiCategory
import com.pascaldornfeld.kitchenapp.util.SingleLiveEvent
import com.pascaldornfeld.kitchenapp.util.StringResourceResolver
import kotlinx.coroutines.launch

class OverviewViewModel(
    private val itemEntityRepository: ItemEntityRepository = Injector.itemEntityRepository,
    private val categoryRepository: CategoryRepository = Injector.categoryRepository,
    private val stringResourceResolver: StringResourceResolver = Injector.stringResourceResolver,
) : ViewModel() {
    private val mutableShoppingListDialogConfig = SingleLiveEvent<ShoppingListDialogConfig>()
    val shoppingListDialogConfig: LiveData<ShoppingListDialogConfig> =
        mutableShoppingListDialogConfig

    private val overviewDataProvider =
        OverviewDataProvider(itemEntityRepository, categoryRepository) { createDefaultCategory() }
    val selectedCategoryName = overviewDataProvider.selectedCategoryName
    val categories = overviewDataProvider.categories
    val currentItemEntities = overviewDataProvider.currentItemEntities

    private fun createDefaultCategory() {
        val defaultCategoryName = stringResourceResolver.resolveString(R.string.default_category)
        viewModelScope.launch {
            categoryRepository.createIfNotExists(Category(defaultCategoryName))
        }
    }

    fun deleteItem(entity: ItemEntity) {
        viewModelScope.launch { itemEntityRepository.delete(entity.name) }
    }

    fun useItem(entity: ItemEntity) {
        val first = entity.getFirstExpiringMhd()
        if (first != null) {
            val entityWithRemovedMhd = entity.minusMhd(first)
            if (entityWithRemovedMhd.getFirstExpiringMhd() == null)
                showDialogForNoMhdLeft(entityWithRemovedMhd)
            else
                viewModelScope.launch {
                    itemEntityRepository.update(entityWithRemovedMhd.name, entityWithRemovedMhd)
                }
        } else throw RuntimeException("there is no first arrayListMhd")
    }

    private fun showDialogForNoMhdLeft(entityWithRemovedMhd: ItemEntity) {
        val entityIsInShoppingList =
            selectedCategoryName.value ==
                stringResourceResolver.resolveString(R.string.shopping_category)

        val dialogConfig =
            if (entityIsInShoppingList)
                ShoppingListDialogConfig.createForDelete(entityWithRemovedMhd)
            else ShoppingListDialogConfig.createForMoveToShoppingList(entityWithRemovedMhd)

        mutableShoppingListDialogConfig.postValue(dialogConfig)
    }

    fun moveToShoppingList(entity: ItemEntity) {
        val entityInShoppingList =
            entity.copy(
                categoryName = stringResourceResolver.resolveString(R.string.shopping_category)
            )
        viewModelScope.launch { itemEntityRepository.update(entity.name, entityInShoppingList) }
    }

    fun setActiveCategory(cat: UiCategory) {
        overviewDataProvider.selectCategory(cat)
    }
}
