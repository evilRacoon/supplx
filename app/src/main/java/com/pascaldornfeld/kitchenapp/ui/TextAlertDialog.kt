package com.pascaldornfeld.kitchenapp.ui

import androidx.compose.material3.AlertDialog
import androidx.compose.material3.Text
import androidx.compose.material3.TextButton
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.PreviewLightDark
import com.pascaldornfeld.kitchenapp.ui.theme.AppTheme

@Composable
fun TextAlertDialog(
    title: String,
    text: String,
    close: () -> Unit,
    onConfirm: () -> Unit,
    modifier: Modifier = Modifier,
) {
    AlertDialog(
        modifier = modifier,
        onDismissRequest = { close() },
        confirmButton = {
            TextButton(
                onClick = {
                    onConfirm()
                    close()
                }
            ) {
                Text(stringResource(android.R.string.ok))
            }
        },
        dismissButton = {
            TextButton(onClick = { close() }) { Text(stringResource(android.R.string.cancel)) }
        },
        title = { Text(title) },
        text = { Text(text) },
    )
}

@PreviewLightDark
@Composable
private fun TextAlertDialogPreview() {
    AppTheme {
        TextAlertDialog(
            title = "Title",
            text = "This is the text of the dialog",
            close = {},
            onConfirm = {},
        )
    }
}
