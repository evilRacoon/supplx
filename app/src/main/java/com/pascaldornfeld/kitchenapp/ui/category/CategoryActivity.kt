@file:OptIn(ExperimentalMaterial3Api::class)

package com.pascaldornfeld.kitchenapp.ui.category

import android.os.Bundle
import android.widget.Toast
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.activity.viewModels
import androidx.compose.foundation.background
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.width
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.LazyItemScope
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.automirrored.filled.ArrowBack
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.icons.filled.Delete
import androidx.compose.material.icons.filled.Edit
import androidx.compose.material.icons.filled.KeyboardArrowDown
import androidx.compose.material.icons.filled.KeyboardArrowUp
import androidx.compose.material3.Card
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.FloatingActionButton
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SwipeToDismissBox
import androidx.compose.material3.SwipeToDismissBoxValue
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBar
import androidx.compose.material3.rememberSwipeToDismissBoxState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.remember
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.PreviewLightDark
import androidx.compose.ui.unit.dp
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.di.Injector
import com.pascaldornfeld.kitchenapp.domain.model.Category
import com.pascaldornfeld.kitchenapp.ui.EditTextAlertDialog
import com.pascaldornfeld.kitchenapp.ui.TextAlertDialog
import com.pascaldornfeld.kitchenapp.ui.theme.AppTheme

class CategoryActivity : ComponentActivity() {
    private val categoryViewModel by
        viewModels<CategoryViewModel> {
            CategoryViewModel.Companion.Factory(Injector.categoryRepository)
        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        categoryViewModel.errorMessage.observe(this) {
            Toast.makeText(this, getString(R.string.name_already_taken), Toast.LENGTH_SHORT).show()
        }
        setContent {
            val categories by categoryViewModel.categories.observeAsState(emptyList())

            CategoryScreen(
                categories = categories,
                create = { categoryViewModel.createIfNotExists(Category(it)) },
                delete = { categoryViewModel.delete(it) },
                rename = { category, name -> categoryViewModel.changeName(category.name, name) },
                switchOrder = { category: Category, category2: Category ->
                    categoryViewModel.swapSort(category, category2)
                },
                onBackClick = { finish() },
            )
        }
    }
}

@Composable
private fun CategoryScreen(
    categories: List<Category>,
    create: (String) -> Unit,
    delete: (Category) -> Unit,
    rename: (Category, String) -> Unit,
    switchOrder: (Category, Category) -> Unit,
    onBackClick: () -> Unit,
) {
    var createDialog by remember { mutableStateOf(false) }

    AppTheme {
        Scaffold(
            topBar = {
                TopAppBar(
                    title = { Text(stringResource(R.string.edit_categories)) },
                    navigationIcon = {
                        IconButton(onClick = { onBackClick() }) {
                            Icon(
                                imageVector = Icons.AutoMirrored.Filled.ArrowBack,
                                contentDescription = stringResource(R.string.back),
                            )
                        }
                    },
                )
            },
            floatingActionButton = {
                FloatingActionButton(onClick = { createDialog = true }) {
                    Icon(
                        imageVector = Icons.Filled.Add,
                        contentDescription = stringResource(R.string.add_category),
                    )
                }
            },
        ) { paddingValues ->
            Box(Modifier.padding(paddingValues)) {
                var deleteDialog by remember<MutableState<DeleteReason?>> { mutableStateOf(null) }
                var renameDialog by remember<MutableState<RenameReason?>> { mutableStateOf(null) }

                Box {
                    CategoryList(
                        categories = categories,
                        rename = { category: Category -> renameDialog = RenameReason(category) },
                        delete = { category: Category -> deleteDialog = DeleteReason(category) },
                        switchOrder = switchOrder,
                    )

                    ConfirmDeleteDialog(
                        deleteDialog,
                        delete = delete,
                        hideDialog = { deleteDialog = null },
                    )

                    RenameDialog(
                        renameDialog,
                        rename = rename,
                        hideDialog = { renameDialog = null },
                    )
                }

                if (createDialog)
                    EditTextAlertDialog(
                        title = stringResource(R.string.enter_name),
                        close = { createDialog = false },
                        onConfirm = { create(it) },
                    )
            }
        }
    }
}

@Composable
private fun RenameDialog(
    renameReason: RenameReason?,
    rename: (Category, String) -> Unit,
    hideDialog: () -> Unit,
) {
    if (renameReason != null)
        EditTextAlertDialog(
            title = stringResource(R.string.enter_name),
            close = { hideDialog() },
            onConfirm = { rename(renameReason.originalCategory, it) },
            initialText = renameReason.originalCategory.name,
        )
}

@Composable
private fun ConfirmDeleteDialog(
    deleteDialog: DeleteReason?,
    delete: (Category) -> Unit,
    hideDialog: () -> Unit,
) {
    if (deleteDialog != null)
        TextAlertDialog(
            title = stringResource(R.string.delete_question),
            text = stringResource(R.string.category_will_be_deleted),
            close = { hideDialog() },
            onConfirm = { delete(deleteDialog.category) },
        )
}

data class RenameReason(val originalCategory: Category)

data class DeleteReason(val category: Category)

@Composable
private fun CategoryList(
    categories: List<Category>,
    rename: (Category) -> Unit,
    delete: (Category) -> Unit,
    switchOrder: (Category, Category) -> Unit,
) {
    LazyColumn {
        items(key = { categories[it].name }, count = categories.size) { index ->
            val category = categories[index]
            CategoryListItem(
                modifier = Modifier.animateItem(),
                category = category,
                isFirst = index == 0,
                isLast = index == categories.lastIndex,
                editName = { rename(category) },
                delete = { delete(category) },
                moveUp = { switchOrder(category, categories[index - 1]) },
                moveDown = { switchOrder(category, categories[index + 1]) },
            )
        }
    }
}

@Composable
private fun LazyItemScope.CategoryListItem(
    category: Category,
    isFirst: Boolean,
    isLast: Boolean,
    editName: () -> Unit,
    delete: () -> Unit,
    moveUp: () -> Unit,
    moveDown: () -> Unit,
    modifier: Modifier = Modifier,
) {
    val dismissState = rememberSwipeToDismissBoxState()

    when (dismissState.currentValue) {
        SwipeToDismissBoxValue.Settled -> {
            // do nothing
        }
        else -> {
            LaunchedEffect(dismissState) {
                delete()
                dismissState.snapTo(SwipeToDismissBoxValue.Settled)
            }
        }
    }

    SwipeToDismissBox(
        modifier = modifier,
        state = dismissState,
        enableDismissFromEndToStart = false,
        backgroundContent = {
            Card(Modifier.fillParentMaxWidth().padding(8.dp)) {
                Box(Modifier.fillMaxSize().background(Color.Red)) {
                    Icon(
                        modifier = Modifier.padding(20.dp).align(Alignment.CenterStart),
                        imageVector = Icons.Filled.Delete,
                        contentDescription = stringResource(R.string.delete),
                    )
                }
            }
        },
        content = {
            Card(Modifier.fillParentMaxWidth().padding(8.dp)) {
                Row(
                    verticalAlignment = Alignment.CenterVertically,
                    modifier = Modifier.padding(12.dp),
                ) {
                    IconButton(onClick = { editName() }) {
                        Icon(
                            modifier = Modifier.padding(8.dp),
                            imageVector = Icons.Filled.Edit,
                            contentDescription = stringResource(R.string.edit_category_description),
                        )
                    }
                    Text(modifier = Modifier.weight(1f), text = category.name)

                    if (!isFirst)
                        IconButton(onClick = { moveUp() }) {
                            Icon(
                                modifier = Modifier.padding(8.dp),
                                imageVector = Icons.Filled.KeyboardArrowUp,
                                contentDescription = stringResource(R.string.sort_up_description),
                            )
                        }

                    if (!isLast) {
                        IconButton(onClick = { moveDown() }) {
                            Icon(
                                modifier = Modifier.padding(8.dp),
                                imageVector = Icons.Filled.KeyboardArrowDown,
                                contentDescription = stringResource(R.string.sort_down_description),
                            )
                        }
                    } else Box(modifier = Modifier.width(48.dp))
                }
            }
        },
    )
}

@PreviewLightDark
@Composable
private fun CategoryScreenPreview() {
    CategoryScreen(
        categories = listOf(Category("name"), Category("name2"), Category("name3")),
        create = {},
        delete = {},
        rename = { _, _ -> },
        switchOrder = { _, _ -> },
        onBackClick = {},
    )
}
