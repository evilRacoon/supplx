package com.pascaldornfeld.kitchenapp.ui.edit

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.pascaldornfeld.kitchenapp.R
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import com.pascaldornfeld.kitchenapp.domain.model.MhdPair
import com.pascaldornfeld.kitchenapp.domain.model.MhdPairDateDescComparator
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

class MhdListAdapter(private val model: EditActivityViewModel) :
    ListAdapter<MhdPair, MhdViewHolder>(
        object : DiffUtil.ItemCallback<MhdPair>() {
            override fun areItemsTheSame(oldItem: MhdPair, newItem: MhdPair) =
                oldItem.date == newItem.date

            override fun areContentsTheSame(oldItem: MhdPair, newItem: MhdPair) = oldItem == newItem
        }
    ) {

    override fun submitList(list: List<MhdPair>?) {
        super.submitList(list?.sortedWith(MhdPairDateDescComparator))
    }

    fun submitList(item: ItemEntity) = submitList(item.listOfMhds)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MhdViewHolder =
        MhdViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.edit_card, parent, false)
        )

    override fun onBindViewHolder(holder: MhdViewHolder, position: Int) {
        val mhd = getItem(position)

        holder.tvDate.text = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(mhd.date)

        holder.imSub.setOnClickListener {
            val ie = model.itemEntityToEdit.value!!
            model.itemEntityToEdit.value = ie.minusMhd(mhd.date)
            model.onChangingMhd()
        }

        holder.tvCount.text =
            holder.tvCount.context.resources.getQuantityString(
                R.plurals.x_pieces,
                mhd.count,
                mhd.count,
            )

        holder.imAdd.setOnClickListener {
            val ie = model.itemEntityToEdit.value!!
            model.itemEntityToEdit.value = ie.plusMhd(mhd.date)
            model.onChangingMhd()
        }
    }
}
