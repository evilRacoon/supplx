package com.pascaldornfeld.kitchenapp.ui.overview

import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity

interface CardCallback {
    fun editItemEntity(entity: ItemEntity)

    fun useItemEntity(entity: ItemEntity)

    fun deleteItemEntity(entity: ItemEntity)
}
