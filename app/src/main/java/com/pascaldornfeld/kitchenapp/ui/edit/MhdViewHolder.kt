package com.pascaldornfeld.kitchenapp.ui.edit

import android.view.View
import android.widget.ImageButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.pascaldornfeld.kitchenapp.R

class MhdViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
    val tvDate: TextView = itemView.findViewById(R.id.tv_date)
    val imSub: ImageButton = itemView.findViewById(R.id.im_sub)
    val tvCount: TextView = itemView.findViewById(R.id.tvCnt)
    val imAdd: ImageButton = itemView.findViewById(R.id.im_add)
}
