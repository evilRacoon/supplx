package com.pascaldornfeld.kitchenapp.application

import android.app.Application
import com.pascaldornfeld.kitchenapp.di.Injector
import timber.log.Timber

class SupplxApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        Injector.initialize(this)
        Timber.plant(Injector.timberTree)
    }
}
