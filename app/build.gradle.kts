buildscript { dependencies { classpath("ru.mobileup:code-quality-android:1.2.0") } }

plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    id("org.jetbrains.kotlin.plugin.compose")
}

apply(plugin = "ru.mobileup.code-quality-android")

val appMinSdk: Int by rootProject.extra
val appCompileSdk: Int by rootProject.extra
val appTargetSdk: Int by rootProject.extra

android {
    defaultConfig {
        applicationId = "com.pascaldornfeld.kitchenapp"

        minSdk = appMinSdk
        compileSdk = appCompileSdk
        targetSdk = appTargetSdk

        val versionName2 = "1.5.2"
        versionName = versionName2
        val versionParts: List<String> = versionName2.split(".")
        assert(versionParts.size == 3)
        versionCode =
            versionParts[0].toInt() * 1_000_000 +
                versionParts[1].toInt() * 1_000 +
                versionParts[2].toInt()

        vectorDrawables.useSupportLibrary = true
        testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
    }
    buildTypes {
        getByName("release") {
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro",
            )
        }
        create("staging") {
            initWith(getByName("release"))
            matchingFallbacks += listOf("release")
            signingConfig = signingConfigs.getByName("debug")
        }
    }
    compileOptions {
        targetCompatibility = JavaVersion.VERSION_17
        sourceCompatibility = JavaVersion.VERSION_17
    }
    testOptions { animationsDisabled = true }
    kotlinOptions { jvmTarget = JavaVersion.VERSION_17.toString() }
    buildFeatures {
        viewBinding = true
        compose = true
    }
    lint {
        checkDependencies = true
        abortOnError = false
    }
    namespace = "com.pascaldornfeld.kitchenapp"
}

dependencies {
    implementation(project(":di"))

    implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")

    implementation("androidx.appcompat:appcompat:1.7.0")
    implementation("androidx.constraintlayout:constraintlayout:2.2.1")
    implementation("androidx.cardview:cardview:1.0.0")
    implementation("androidx.recyclerview:recyclerview:1.4.0")
    implementation("androidx.activity:activity-ktx:1.10.1")
    implementation("androidx.lifecycle:lifecycle-livedata-ktx:2.8.7")

    implementation("com.google.android.material:material:1.12.0")
    implementation("com.jakewharton.timber:timber:5.0.1")

    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-android:1.9.0")

    val composeBom = platform("androidx.compose:compose-bom:2025.02.00")
    implementation(composeBom)
    implementation("androidx.compose.material3:material3")
    implementation("androidx.compose.ui:ui-tooling-preview")
    debugImplementation("androidx.compose.ui:ui-tooling")
    implementation("androidx.compose.material:material-icons-extended")
    implementation("androidx.activity:activity-compose:1.10.1")
    implementation("androidx.lifecycle:lifecycle-viewmodel-compose:2.8.7")
    implementation("androidx.compose.runtime:runtime-livedata")

    debugImplementation("com.squareup.leakcanary:leakcanary-android:2.14")

    testImplementation("com.jraska.livedata:testing-ktx:1.3.0")
    testImplementation("androidx.arch.core:core-testing:2.2.0")
    testImplementation("com.google.truth:truth:1.4.4")
    testImplementation("junit:junit:4.13.2")
    testImplementation("io.mockk:mockk:1.13.17")
    testImplementation("org.jetbrains.kotlinx:kotlinx-coroutines-test")
}

configure<ru.mobileup.codequality.CodeQualityExtension> {
    reportsDirectory = file("build/reports")
    issuesFile = rootProject.file("build/code_quality_issues.json")
}
