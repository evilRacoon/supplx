package com.pascaldornfeld.kitchenapp.di

import timber.log.Timber

object TimberTree : Timber.Tree() {
    override fun log(priority: Int, tag: String?, message: String, t: Throwable?) = Unit
}
