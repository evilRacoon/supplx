package com.pascaldornfeld.kitchenapp.di

import timber.log.Timber

object TimberTree : Timber.DebugTree() {
    override fun createStackElementTag(element: StackTraceElement) =
        "${super.createStackElementTag(element)} (${element.lineNumber})"
}
