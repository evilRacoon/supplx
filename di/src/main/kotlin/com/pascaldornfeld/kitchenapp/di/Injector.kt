package com.pascaldornfeld.kitchenapp.di

import android.app.Application
import android.content.Context
import com.pascaldornfeld.kitchenapp.data.repository.CategoryRoomRepository
import com.pascaldornfeld.kitchenapp.data.repository.ItemEntityRoomRepository
import com.pascaldornfeld.kitchenapp.data.room.SupplxRoomDatabase
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.ItemEntityRepository
import com.pascaldornfeld.kitchenapp.util.StringResourceResolver

object Injector {
    lateinit var appContext: Context

    fun initialize(application: Application) {
        appContext = application
    }

    // data

    private val roomDatabase by lazy { SupplxRoomDatabase.createDatabase(appContext) }
    val categoryRepository by lazy<CategoryRepository> { CategoryRoomRepository(roomDatabase) }
    val itemEntityRepository by
        lazy<ItemEntityRepository> { ItemEntityRoomRepository(roomDatabase) }

    // domain

    // util
    val stringResourceResolver by lazy { StringResourceResolver(appContext) }

    val timberTree = TimberTree
}
