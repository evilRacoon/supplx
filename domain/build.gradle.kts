plugins {
    id("java-library")
    id("org.jetbrains.kotlin.jvm")
}

java { toolchain { languageVersion = JavaLanguageVersion.of(17) } }

dependencies {
    api("org.jetbrains.kotlin:kotlin-stdlib")
    api("org.jetbrains.kotlinx:kotlinx-coroutines-core:1.9.0")
    testImplementation("com.google.truth:truth:1.4.4")
}
