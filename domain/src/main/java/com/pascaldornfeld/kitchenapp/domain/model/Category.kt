package com.pascaldornfeld.kitchenapp.domain.model

data class Category(val name: String, val sortId: Long = 0)
