package com.pascaldornfeld.kitchenapp.domain.model

import java.time.LocalDate

/** Represents items of the same type that expire at the same date */
data class MhdPair(val date: LocalDate, val count: Int)
