package com.pascaldornfeld.kitchenapp.domain

import com.pascaldornfeld.kitchenapp.domain.model.Category
import kotlinx.coroutines.flow.Flow

interface CategoryRepository {
    suspend fun createIfNotExists(category: Category)

    fun getAllAscAsync(): Flow<List<Category>>

    suspend fun getAllAsc(): List<Category>

    suspend fun changeName(oldName: String, newName: String)

    suspend fun delete(category: Category)

    suspend fun swapSort(category1: Category, category2: Category)
}
