package com.pascaldornfeld.kitchenapp.domain.model

import com.pascaldornfeld.kitchenapp.domain.model.ItemEntityComparison.Importance.BothAreSameImportant
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntityComparison.Importance.FirstIsMoreImportant
import java.time.LocalDate

/** Represents all items of the same type */
data class ItemEntity(
    val name: String,
    val categoryName: String,
    val listOfMhds: List<MhdPair> = emptyList(),
) : Comparable<ItemEntity> {

    fun plusMhd(dateOfMhdToBeAdded: LocalDate): ItemEntity {
        var dateExists = false
        val newMhdList = mutableListOf<MhdPair>()
        listOfMhds.forEach { existingMhd ->
            if (existingMhd.date == dateOfMhdToBeAdded) {
                dateExists = true
                newMhdList.add(existingMhd.copy(count = existingMhd.count + 1))
            } else newMhdList.add(existingMhd.copy())
        }
        if (!dateExists) newMhdList.add(MhdPair(dateOfMhdToBeAdded, 1))
        return copy(listOfMhds = newMhdList)
    }

    fun minusMhd(d: LocalDate): ItemEntity {
        val newMhdList = mutableListOf<MhdPair>()
        for (mhd in listOfMhds) {
            if (mhd.date == d) {
                if (mhd.count > 1) newMhdList.add(mhd.copy(count = mhd.count - 1))
            } else newMhdList.add(mhd.copy())
        }
        return copy(listOfMhds = newMhdList)
    }

    fun getFirstExpiringMhd(): LocalDate? = listOfMhds.minOfOrNull { it.date }

    fun getCount() = listOfMhds.sumOf { it.count }

    override fun compareTo(other: ItemEntity): Int {
        val comparison = ItemEntityComparison(this, other)
        return when (comparison.result) {
            BothAreSameImportant -> 0
            FirstIsMoreImportant -> -1
            else -> 1
        }
    }
}
