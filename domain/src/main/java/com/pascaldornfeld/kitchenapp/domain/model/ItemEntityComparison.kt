package com.pascaldornfeld.kitchenapp.domain.model

import com.pascaldornfeld.kitchenapp.domain.model.ItemEntityComparison.Importance.BothAreSameImportant
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntityComparison.Importance.FirstIsMoreImportant
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntityComparison.Importance.SecondIsMoreImportant
import kotlin.math.max

class ItemEntityComparison(
    private val firstEntity: ItemEntity,
    private val secondEntity: ItemEntity,
) {
    enum class Importance {
        FirstIsMoreImportant,
        SecondIsMoreImportant,
        BothAreSameImportant,
    }

    val result = getImportance()

    private fun getImportance(): Importance {
        val mhdImportance = getMhdsImportance()
        if (mhdImportance != BothAreSameImportant) return mhdImportance
        val nameImportance = getNameImportance()
        if (nameImportance != BothAreSameImportant) return nameImportance
        val categoryImportance = getCategoryImportance()
        if (categoryImportance != BothAreSameImportant) return categoryImportance
        return BothAreSameImportant
    }

    private fun getMhdsImportance(): Importance {
        val sortedMhdsOfFirst = firstEntity.listOfMhds.sortedBy { it.date }
        val sortedMhdsOfSecond = secondEntity.listOfMhds.sortedBy { it.date }
        val biggerSize = max(sortedMhdsOfFirst.size, sortedMhdsOfSecond.size)
        for (i in 0 until biggerSize) {
            val mhdOfFirst = sortedMhdsOfFirst.getOrNull(i)
            val mhdOfSecond = sortedMhdsOfSecond.getOrNull(i)
            val mhdImportance = getMhdImportance(mhdOfFirst, mhdOfSecond)
            if (mhdImportance != BothAreSameImportant) return mhdImportance
        }
        return BothAreSameImportant
    }

    private fun getMhdImportance(firstMhd: MhdPair?, secondMhd: MhdPair?): Importance {
        return when {
            firstMhd == secondMhd -> BothAreSameImportant
            firstMhd == null -> SecondIsMoreImportant
            secondMhd == null -> FirstIsMoreImportant
            firstMhd.date.isBefore(secondMhd.date) -> FirstIsMoreImportant
            firstMhd.date.isAfter(secondMhd.date) -> SecondIsMoreImportant
            firstMhd.count > secondMhd.count -> FirstIsMoreImportant
            firstMhd.count < secondMhd.count -> SecondIsMoreImportant
            else -> BothAreSameImportant
        }
    }

    private fun getCategoryImportance(): Importance {
        val compareResult = firstEntity.categoryName.compareTo(secondEntity.categoryName)
        return getImportanceFromCompareResult(compareResult)
    }

    private fun getNameImportance(): Importance {
        val compareResult = firstEntity.name.compareTo(secondEntity.name)
        return getImportanceFromCompareResult(compareResult)
    }

    private fun getImportanceFromCompareResult(compareResult: Int) =
        when {
            compareResult == 0 -> BothAreSameImportant
            compareResult < 0 -> FirstIsMoreImportant
            else -> SecondIsMoreImportant
        }
}
