package com.pascaldornfeld.kitchenapp.domain.model

/** Compares [MhdPair]s by date descending */
object MhdPairDateDescComparator : Comparator<MhdPair> {
    override fun compare(o1: MhdPair?, o2: MhdPair?): Int =
        if (o1 == null || o2 == null) 0 else -o1.date.compareTo(o2.date)
}
