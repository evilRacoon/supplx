package com.pascaldornfeld.kitchenapp.domain

import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import kotlinx.coroutines.flow.Flow

interface ItemEntityRepository {
    suspend fun save(itemEntity: ItemEntity)

    suspend fun getByName(name: String): ItemEntity?

    fun getAllAsync(): Flow<List<ItemEntity>>

    suspend fun getAll(): List<ItemEntity>

    suspend fun update(oldName: String, updatedItemEntity: ItemEntity)

    suspend fun delete(name: String)
}
