package com.pascaldornfeld.kitchenapp.domain.model

import com.google.common.truth.Truth.assertThat
import java.time.LocalDate
import org.junit.Test

class ItemEntityTest {
    @Test
    fun getCount_emptyItemEntity_shouldReturnZero() {
        val uut = ItemEntity("Unit Under Test", "Default")
        assertThat(uut.getCount()).isEqualTo(0)
    }

    @Test
    fun getCount_shouldReturnCountOfItems() {
        val uut =
            ItemEntity("Unit Under Test", "Default")
                .plusMhd(LocalDate.of(2018, 6, 6))
                .plusMhd(LocalDate.of(2018, 6, 6))
                .plusMhd(LocalDate.of(2018, 6, 8))
        assertThat(uut.getCount()).isEqualTo(3)
    }

    @Test
    fun minusMhd_shouldRemoveItem() {
        val uut =
            ItemEntity("Unit Under Test", "Default")
                .plusMhd(LocalDate.of(2018, 6, 6))
                .minusMhd(LocalDate.of(2018, 6, 6))
        assertThat(uut.getCount()).isEqualTo(0)
    }

    @Test
    fun minusMhd_notExistingMhd_shouldIgnoreCall() {
        val uut = ItemEntity("Unit Under Test", "Default").minusMhd(LocalDate.of(2018, 6, 6))
        assertThat(uut.getCount()).isEqualTo(0)
    }

    @Test
    fun getFirstExpiringMhd_shouldReturnEarliestDate() {
        val uut =
            ItemEntity("Unit Under Test", "Default")
                .plusMhd(LocalDate.of(2018, 6, 6))
                .plusMhd(LocalDate.of(2018, 6, 4))
                .plusMhd(LocalDate.of(2018, 6, 6))
        assertThat(uut.getFirstExpiringMhd()).isEqualTo(LocalDate.of(2018, 6, 4))
    }

    @Test
    fun getFirstExpiringMhd_noItems_shouldReturnNull() {
        val uut = ItemEntity("Unit Under Test", "Default")
        assertThat(uut.getFirstExpiringMhd()).isNull()
    }

    @Test
    fun entity_shouldBeImmutable() {
        val uut1 = ItemEntity("Unit Under Test 1", "Default")

        uut1.plusMhd(LocalDate.of(2018, 6, 6))
        uut1.plusMhd(LocalDate.of(2018, 6, 8))
        uut1.minusMhd(LocalDate.of(2018, 6, 6))
        uut1.getFirstExpiringMhd()
        uut1.getCount()

        assertThat(uut1).isEqualTo(ItemEntity("Unit Under Test 1", "Default"))
    }

    @Test
    fun compare_sameItem_shouldBeSame() {
        val uut1 = ItemEntity("Unit Under Test 1", "Default")
        val uut2 = ItemEntity("Unit Under Test 1", "Default")
        assertThat(uut1.compareTo(uut2)).isEqualTo(0)
    }

    @Test
    fun compare_sameItemWithMhd_shouldBeSame() {
        val uut1 = ItemEntity("Unit Under Test 1", "Default").plusMhd(LocalDate.of(2020, 1, 1))
        val uut2 = ItemEntity("Unit Under Test 1", "Default").plusMhd(LocalDate.of(2020, 1, 1))
        assertThat(uut1.compareTo(uut2)).isEqualTo(0)
    }

    @Test
    fun compare_earlierExpiring_shouldBeEarlier() {
        val uut1 = ItemEntity("Unit Under Test 1", "Default").plusMhd(LocalDate.of(2020, 1, 1))
        val uut2 =
            ItemEntity("Unit Under Test 1", "Default")
                .plusMhd(LocalDate.of(2020, 1, 2))
                .plusMhd(LocalDate.of(2020, 1, 2))
                .plusMhd(LocalDate.of(2020, 1, 3))
        assertThat(uut1.compareTo(uut2)).isLessThan(0)
    }

    @Test
    fun compare_laterExpiring_shouldBeLater() {
        val uut1 = ItemEntity("Unit Under Test 1", "Default").plusMhd(LocalDate.of(2020, 1, 5))
        val uut2 = ItemEntity("Unit Under Test 1", "Default").plusMhd(LocalDate.of(2020, 1, 2))
        assertThat(uut1.compareTo(uut2)).isGreaterThan(0)
    }

    @Test
    fun compare_moreCountAtMhd_shouldBeEarlier() {
        val uut1 =
            ItemEntity("Unit Under Test 1", "Default")
                .plusMhd(LocalDate.of(2020, 1, 5))
                .plusMhd(LocalDate.of(2020, 1, 5))
        val uut2 =
            ItemEntity("Unit Under Test 1", "Default")
                .plusMhd(LocalDate.of(2020, 1, 5))
                .plusMhd(LocalDate.of(2020, 1, 8))
                .plusMhd(LocalDate.of(2020, 1, 8))
        assertThat(uut1.compareTo(uut2)).isLessThan(0)
    }
}
