package com.pascaldornfeld.kitchenapp.domain.model

import com.google.common.truth.Truth.assertThat
import java.time.LocalDate
import org.junit.Test

class MhdPairDateDescComparatorTest {

    @Test
    fun compare_sameItem_shouldBeSame() {
        val uut1 = MhdPair(LocalDate.of(2020, 1, 1), 1)
        val uut2 = MhdPair(LocalDate.of(2020, 1, 1), 1)
        assertThat(MhdPairDateDescComparator.compare(uut1, uut2)).isEqualTo(0)
    }

    @Test
    fun compare_sameDateDifferentCount_shouldBeSame() {
        val uut1 = MhdPair(LocalDate.of(2020, 1, 1), 4)
        val uut2 = MhdPair(LocalDate.of(2020, 1, 1), 1)
        assertThat(MhdPairDateDescComparator.compare(uut1, uut2)).isEqualTo(0)
    }

    @Test
    fun compare_later_shouldReturnGreater() {
        val uut1 = MhdPair(LocalDate.of(2020, 1, 1), 4)
        val uut2 = MhdPair(LocalDate.of(2020, 1, 2), 1)
        assertThat(MhdPairDateDescComparator.compare(uut1, uut2)).isGreaterThan(0)
    }

    @Test
    fun compare_earlier_shouldReturnSmaller() {
        val uut1 = MhdPair(LocalDate.of(2020, 1, 2), 4)
        val uut2 = MhdPair(LocalDate.of(2020, 1, 1), 1)
        assertThat(MhdPairDateDescComparator.compare(uut1, uut2)).isLessThan(0)
    }
}
