module.exports = {
  "extends": [
    "group:monorepos",
    "group:recommended",
    "workarounds:all"
  ],
  "platform": "gitlab",
  "onboarding": false,
  "pinDigests": true,
  "fetchChangeLogs": "off",
  "labels": [
    "renovate"
  ],
  "requireConfig": "optional",
  "automerge": true,
  "automergeType": "pr",
  "platformAutomerge": true,
  "configMigration": true,
  "dependencyDashboard": true,
  "prConcurrentLimit": 0,
  "prHourlyLimit": 0,
  "packageRules": [
    {
      "matchPackageNames": [
        "openjdk"
      ],
      "matchUpdateTypes": [
        "major"
      ],
      "enabled": false
    },
    {
      "matchDepNames": [
        "debian_12/ruby-bundler"
      ],
      "enabled": false
    },
    {
      "matchDepNames": [
        "debian_12/build-essential"
      ],
      "enabled": false
    }
  ],
  "customManagers": [
    {
      "customType": "regex",
      "fileMatch": [
        "Dockerfile$"
      ],
      "matchStrings": [
        "#\\s*renovate:\\s*datasource=(?<datasource>.*?) depName=(?<depName>.*?)( versioning=(?<versioning>.*?))?\\sENV .*?_VERSION=\"(?<currentValue>.*)\"\\s"
      ],
      "versioningTemplate": "{{#if versioning}}{{{versioning}}}{{else}}semver{{/if}}"
    }
  ]
}