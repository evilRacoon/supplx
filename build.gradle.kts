// Top-level build file where you can add configuration options common to all sub-projects/modules.
plugins {
    val kotlinVersion = "2.0.21"

    id("com.android.application") version "8.9.0" apply false
    id("com.android.library") version "8.9.0" apply false
    id("org.jetbrains.kotlin.android") version kotlinVersion apply false
    id("org.jetbrains.kotlin.jvm") version kotlinVersion apply false
    id("org.jetbrains.kotlin.plugin.compose") version kotlinVersion apply false
    id("com.google.devtools.ksp") version "${kotlinVersion}-1.0.25" apply false
    id("org.sonarqube") version "6.0.1.5171"
}

extra.apply {
    set("appMinSdk", 26)
    set("appCompileSdk", 35)
    set("appTargetSdk", 34)
}

sonar {
    properties {
        property("sonar.projectKey", "evilRacoon_supplx")
        property("sonar.organization", "evilracoon")
        property("sonar.host.url", "https://sonarcloud.io")
        property("sonar.coverage.jacoco.xmlReportPaths", "${rootDir}/build/reports/kover/report.xml")
        property("sonar.qualitygate.wait", "true")
    }
}

tasks.register("clean", Delete::class) {
    description = "Cleans all build files"
    group = "build"
    delete(layout.buildDirectory)
}
