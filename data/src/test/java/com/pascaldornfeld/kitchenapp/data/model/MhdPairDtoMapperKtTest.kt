package com.pascaldornfeld.kitchenapp.data.model

import com.google.common.truth.Truth.assertThat
import com.pascaldornfeld.kitchenapp.domain.model.MhdPair
import java.time.LocalDate
import java.time.Month
import java.util.Calendar
import java.util.GregorianCalendar
import org.junit.Test

class MhdPairDtoMapperKtTest {
    @Test
    fun map_shouldMapToData() {
        val domainModel = MhdPair(LocalDate.of(2020, Month.APRIL, 3), 1)
        val dataModel = domainModel.toDataModel()
        val date = dataModel.date

        assertThat(date.get(Calendar.YEAR)).isEqualTo(2020)
        assertThat(date.get(Calendar.MONTH)).isEqualTo(Calendar.APRIL)
        assertThat(date.get(Calendar.DAY_OF_MONTH)).isEqualTo(3)
    }

    @Test
    fun map_shouldMapToDomain() {
        val dataModel = MhdPairDto(GregorianCalendar(2020, Calendar.APRIL, 3), 1)
        val domainModel = dataModel.toDomainModel()
        val date = domainModel.date

        assertThat(date.year).isEqualTo(2020)
        assertThat(date.month).isEqualTo(Month.APRIL)
        assertThat(date.dayOfMonth).isEqualTo(3)
    }
}
