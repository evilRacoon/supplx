package com.pascaldornfeld.kitchenapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(tableName = "Category", indices = [(Index(value = ["sort_id"], unique = true))])
data class CategoryDto(
    @PrimaryKey @ColumnInfo(name = "name") val name: String,
    @ColumnInfo(name = "sort_id") val sortId: Long,
)
