package com.pascaldornfeld.kitchenapp.data.repository

import com.pascaldornfeld.kitchenapp.data.model.toDataModel
import com.pascaldornfeld.kitchenapp.data.model.toDomainModel
import com.pascaldornfeld.kitchenapp.data.room.SupplxRoomDatabase
import com.pascaldornfeld.kitchenapp.domain.CategoryRepository
import com.pascaldornfeld.kitchenapp.domain.model.Category
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class CategoryRoomRepository(private val roomDatabase: SupplxRoomDatabase) : CategoryRepository {
    override suspend fun createIfNotExists(category: Category) =
        roomDatabase.daoCategory.saveCategory(category.toDataModel())

    override fun getAllAscAsync(): Flow<List<Category>> =
        roomDatabase.daoCategory.getCategoriesAscendingAsync().map { it.map { it.toDomainModel() } }

    override suspend fun getAllAsc(): List<Category> =
        roomDatabase.daoCategory.getCategoriesAscending().map { it.toDomainModel() }

    override suspend fun changeName(oldName: String, newName: String) =
        roomDatabase.daoCategory.changeCategoryName(oldName, newName)

    override suspend fun delete(category: Category) =
        roomDatabase.daoCategory.deleteCategory(category.toDataModel())

    override suspend fun swapSort(category1: Category, category2: Category) =
        roomDatabase.daoCategory.swapCategorySort(category1.toDataModel(), category2.toDataModel())
}
