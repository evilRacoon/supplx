package com.pascaldornfeld.kitchenapp.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "ItemEntity",
    foreignKeys =
        [
            (ForeignKey(
                entity = CategoryDto::class,
                parentColumns = arrayOf("name"),
                childColumns = arrayOf("category_name"),
                onDelete = ForeignKey.CASCADE,
                onUpdate = ForeignKey.CASCADE,
            ))
        ],
    indices = [(Index(value = ["category_name"]))],
)
data class ItemEntityDto(
    @PrimaryKey @ColumnInfo(name = "item_name") val name: String,
    @ColumnInfo(name = "category_name") val categoryName: String,
    @ColumnInfo(name = "mhds") val arrayListMhd: List<MhdPairDto>,
)
