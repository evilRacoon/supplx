package com.pascaldornfeld.kitchenapp.data.model

import com.pascaldornfeld.kitchenapp.domain.model.Category

fun Category.toDataModel() = CategoryDto(this.name, this.sortId)

fun CategoryDto.toDomainModel() = Category(this.name, this.sortId)
