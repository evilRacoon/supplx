package com.pascaldornfeld.kitchenapp.data.model

import com.pascaldornfeld.kitchenapp.domain.model.MhdPair
import java.time.LocalDate
import java.util.Calendar
import java.util.GregorianCalendar

fun MhdPair.toDataModel() =
    MhdPairDto(GregorianCalendar(date.year, date.monthValue - 1, date.dayOfMonth), this.count)

fun MhdPairDto.toDomainModel() =
    MhdPair(
        LocalDate.of(
            this.date[Calendar.YEAR],
            this.date[Calendar.MONTH] + 1,
            this.date[Calendar.DAY_OF_MONTH],
        ),
        this.count,
    )
