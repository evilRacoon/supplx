package com.pascaldornfeld.kitchenapp.data.room.dao

import android.database.sqlite.SQLiteConstraintException
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Transaction
import com.pascaldornfeld.kitchenapp.data.model.CategoryDto
import kotlinx.coroutines.flow.Flow
import timber.log.Timber

@Dao
abstract class DaoCategory {
    @Query("SELECT * FROM Category ORDER BY sort_id ASC")
    abstract suspend fun getCategoriesAscending(): List<CategoryDto>

    @Query("SELECT * FROM Category ORDER BY sort_id ASC")
    abstract fun getCategoriesAscendingAsync(): Flow<List<CategoryDto>>

    @Query("UPDATE Category SET name = :newName WHERE name = :oldName")
    abstract suspend fun changeCategoryName(oldName: String, newName: String)

    @Query("SELECT * FROM Category WHERE name = :categoryName")
    abstract suspend fun getCategoryByName(categoryName: String): CategoryDto?

    @Transaction
    open suspend fun swapCategorySort(cat1: CategoryDto, cat2: CategoryDto) {
        updateCategorySortId(cat2.name, -1)
        updateCategorySortId(cat1.name, cat2.sortId)
        updateCategorySortId(cat2.name, cat1.sortId)
    }

    @Transaction
    open suspend fun saveCategory(category: CategoryDto) {
        require(category.sortId == 0L) { "SortID of Category is not 0. Did you want to update?" }
        if (getCategoryByName(category.name) == null) {
            var maxId = getMaxSortID()
            check(maxId != Long.MAX_VALUE) { "Too many categories saved" }
            if (maxId == null) maxId = 0 else maxId++
            val categoryToSave = category.copy(sortId = maxId)
            Timber.d("Saved category $categoryToSave")
            saveCategoryUnchecked(categoryToSave)
        } else {
            Timber.d("Category exists already")
        }
    }

    @Transaction
    open suspend fun deleteCategory(category: CategoryDto) {
        val begin = category.sortId + 1
        val end = getMaxSortID() ?: throw SQLiteConstraintException("WTF")
        deleteCategoryUnchecked(category.name)
        for (i in begin..end) decreaseSortId(i)
    }

    @Query("UPDATE Category SET sort_id = sort_id - 1 WHERE sort_id == :id")
    protected abstract suspend fun decreaseSortId(id: Long)

    @Query("DELETE FROM Category WHERE name = :categoryName")
    protected abstract suspend fun deleteCategoryUnchecked(categoryName: String)

    @Query("SELECT MAX(sort_id) FROM Category") protected abstract suspend fun getMaxSortID(): Long?

    @Query("SELECT COUNT(name) FROM Category") abstract suspend fun countOfCats(): Int

    @Insert protected abstract suspend fun saveCategoryUnchecked(category: CategoryDto)

    @Query("UPDATE Category SET sort_id = :sortId WHERE name = :categoryName")
    protected abstract suspend fun updateCategorySortId(categoryName: String, sortId: Long)
}
