package com.pascaldornfeld.kitchenapp.data.repository

import androidx.room.withTransaction
import com.pascaldornfeld.kitchenapp.data.model.CategoryDto
import com.pascaldornfeld.kitchenapp.data.model.toDataModel
import com.pascaldornfeld.kitchenapp.data.model.toDomainModel
import com.pascaldornfeld.kitchenapp.data.room.SupplxRoomDatabase
import com.pascaldornfeld.kitchenapp.domain.ItemEntityRepository
import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map

class ItemEntityRoomRepository(private val roomDatabase: SupplxRoomDatabase) :
    ItemEntityRepository {
    override suspend fun save(itemEntity: ItemEntity) =
        roomDatabase.withTransaction { saveItemEntityAndCreateItsCategories(itemEntity) }

    override suspend fun getByName(name: String) =
        roomDatabase.daoItemEntity.getItemWithName(name)?.toDomainModel()

    override fun getAllAsync(): Flow<List<ItemEntity>> =
        roomDatabase.daoItemEntity.getAllItemsAsync().map {
            it.map {
                ItemEntity(it.name, it.categoryName, it.arrayListMhd.map { it.toDomainModel() })
            }
        }

    override suspend fun getAll(): List<ItemEntity> =
        roomDatabase.daoItemEntity.getAllItems().map { it.toDomainModel() }

    override suspend fun update(oldName: String, updatedItemEntity: ItemEntity) =
        roomDatabase.withTransaction {
            roomDatabase.daoItemEntity.deleteItem(oldName)
            saveItemEntityAndCreateItsCategories(updatedItemEntity)
        }

    override suspend fun delete(name: String) = roomDatabase.daoItemEntity.deleteItem(name)

    private suspend fun saveItemEntityAndCreateItsCategories(itemEntity: ItemEntity) {
        with(roomDatabase) {
            if (daoCategory.getCategoryByName(itemEntity.categoryName) == null)
                daoCategory.saveCategory(CategoryDto(itemEntity.categoryName, 0))
            daoItemEntity.saveItem(itemEntity.toDataModel())
        }
    }
}
