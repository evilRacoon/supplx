package com.pascaldornfeld.kitchenapp.data.model

import androidx.annotation.Keep
import com.google.gson.annotations.SerializedName
import java.util.*

@Keep
data class MhdPairDto(@SerializedName("b") val date: Calendar, @SerializedName("c") val count: Int)
