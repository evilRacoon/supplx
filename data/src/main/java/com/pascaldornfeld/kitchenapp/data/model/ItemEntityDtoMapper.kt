package com.pascaldornfeld.kitchenapp.data.model

import com.pascaldornfeld.kitchenapp.domain.model.ItemEntity

fun ItemEntity.toDataModel() =
    ItemEntityDto(this.name, this.categoryName, this.listOfMhds.map { it.toDataModel() })

fun ItemEntityDto.toDomainModel() =
    ItemEntity(this.name, this.categoryName, this.arrayListMhd.map { it.toDomainModel() })
