package com.pascaldornfeld.kitchenapp.data.room.converter

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.pascaldornfeld.kitchenapp.data.model.MhdPairDto

object Converters {
    private val gson = Gson()

    @JvmStatic
    @TypeConverter
    fun fromMhdPairs(pair: List<MhdPairDto?>?): String {
        return gson.toJson(pair)
    }

    @JvmStatic
    @TypeConverter
    fun toMhdPairs(json: String?): List<MhdPairDto> {
        if (json == null) return ArrayList()
        val listType = object : TypeToken<List<MhdPairDto?>?>() {}.type
        return gson.fromJson(json, listType)
    }
}
