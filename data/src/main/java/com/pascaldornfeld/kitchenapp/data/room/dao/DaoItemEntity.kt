package com.pascaldornfeld.kitchenapp.data.room.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.pascaldornfeld.kitchenapp.data.model.ItemEntityDto
import kotlinx.coroutines.flow.Flow

@Dao
abstract class DaoItemEntity {
    @Query("SELECT * FROM ItemEntity") abstract fun getAllItemsAsync(): Flow<List<ItemEntityDto>>

    @Query("SELECT * FROM ItemEntity") abstract suspend fun getAllItems(): List<ItemEntityDto>

    @Query("SELECT * FROM ItemEntity WHERE item_name = :itemName")
    abstract suspend fun getItemWithName(itemName: String): ItemEntityDto?

    @Insert abstract suspend fun saveItem(itemEntity: ItemEntityDto)

    @Query("DELETE FROM ItemEntity WHERE item_name = :itemName")
    abstract suspend fun deleteItem(itemName: String)
}
