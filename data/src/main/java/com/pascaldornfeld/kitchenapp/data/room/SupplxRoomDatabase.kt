package com.pascaldornfeld.kitchenapp.data.room

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.pascaldornfeld.kitchenapp.data.model.CategoryDto
import com.pascaldornfeld.kitchenapp.data.model.ItemEntityDto
import com.pascaldornfeld.kitchenapp.data.room.converter.Converters
import com.pascaldornfeld.kitchenapp.data.room.dao.DaoCategory
import com.pascaldornfeld.kitchenapp.data.room.dao.DaoItemEntity
import java.io.Closeable

@Database(entities = [(ItemEntityDto::class), (CategoryDto::class)], version = 1)
@TypeConverters(Converters::class)
abstract class SupplxRoomDatabase : RoomDatabase(), Closeable {
    abstract val daoCategory: DaoCategory
    abstract val daoItemEntity: DaoItemEntity

    override fun close() {
        super.close()
    }

    companion object {
        fun createDatabase(context: Context): SupplxRoomDatabase {
            return Room.databaseBuilder(
                    context.applicationContext,
                    SupplxRoomDatabase::class.java,
                    "kitchenapp.db",
                )
                .build()
        }
    }
}
