package com.pascaldornfeld.kitchenapp.util

import android.content.Context
import androidx.annotation.StringRes

class StringResourceResolver(private val context: Context) {
    fun resolveString(@StringRes id: Int): String = context.getString(id)
}
