package com.pascaldornfeld.kitchenapp.util

import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData

fun <A, B, R> LiveData<A>.combineWith(liveData: LiveData<B>, block: (A?, B?) -> R): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) { result.value = block(this.value, liveData.value) }
    result.addSource(liveData) { result.value = block(this.value, liveData.value) }
    return result
}

fun <A, B, C, R> LiveData<A>.combineWith(
    liveData: LiveData<B>,
    liveData2: LiveData<C>,
    block: (A?, B?, C?) -> R,
): LiveData<R> {
    val result = MediatorLiveData<R>()
    result.addSource(this) { result.value = block(this.value, liveData.value, liveData2.value) }
    result.addSource(liveData) { result.value = block(this.value, liveData.value, liveData2.value) }
    result.addSource(liveData2) {
        result.value = block(this.value, liveData.value, liveData2.value)
    }
    return result
}
