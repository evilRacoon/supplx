fastlane documentation
----

# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```sh
xcode-select --install
```

For _fastlane_ installation instructions, see [Installing _fastlane_](https://docs.fastlane.tools/#installing-fastlane)

# Available Actions

## Android

### android test

```sh
[bundle exec] fastlane android test
```

Runs unit tests

### android uitest_emulator

```sh
[bundle exec] fastlane android uitest_emulator
```

Runs ui tests on emulator

### android uitest_cloud

```sh
[bundle exec] fastlane android uitest_cloud
```

Runs maestro tests on cloud

### android build_signed_release_app

```sh
[bundle exec] fastlane android build_signed_release_app
```

Builds release flavor apk and aab and signs it with keystore

### android release

```sh
[bundle exec] fastlane android release
```

Raises version, adds git tag and deploys to play store

----

This README.md is auto-generated and will be re-generated every time [_fastlane_](https://fastlane.tools) is run.

More information about _fastlane_ can be found on [fastlane.tools](https://fastlane.tools).

The documentation of _fastlane_ can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
